﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships
{
    class Player
    {
        //Attributes.
        private int shotsHit;
        private int shotsMissed;

        //Constructor.
        public Player()
        {
            //Set default values for attributes.
            shotsHit = 0;
            shotsMissed = 0;
        }
        //Retrieve number of shots hit by player.
        public int getShotsHit()
        {
            return this.shotsHit;
        }
        //Retrieve number of shots missed by player.
        public int getShotsMissed()
        {
            return this.shotsMissed;
        }
        public char[] convertShotPlacementToChar(string inputShot)
        {
            //Convert the string input from the user into a character array.
            char[] charArray = inputShot.ToCharArray();
            
            //As the first character is a letter, we need to convert this to a number equivalent for the grid.
            switch (charArray[0])
            {
                    case 'a':
                        charArray[0] = '0';
                        break;
                    case 'b':
                        charArray[0] = '1';
                        break;
                    case 'c':
                        charArray[0] = '2';
                        break;
                    case 'd':
                        charArray[0] = '3';
                        break;
                    case 'e':
                        charArray[0] = '4';
                        break;
                    case 'f':
                        charArray[0] = '5';
                        break;
                    case 'g':
                        charArray[0] = '6';
                        break;
                    case 'h':
                        charArray[0] = '7';
                        break;
                    case 'i':
                        charArray[0] = '8';
                        break;
                    case 'j':
                        charArray[0] = '9';
                        break;
                    default:
                        Console.WriteLine("Please enter a co-ordinate between A and J for X, and 0-9 for Y.");
                        break;

            }
            //Once the char array has been updated correctly, we return this to the main function.
            return charArray;
        }
        public void fireShot(int x, int y, int[,]grid)
        {
            //Check if the grid in co-ordinates being passed to the function contains a ship number or not.
            if(grid[x,y]!= 0)
            {
                //If it does contain a ship - notify the user.
                Console.WriteLine("\nA ship has been hit!\n");

                //Also update the user's shot counter.
                this.shotsHit = this.shotsHit + 1;

                //Set the position to zero to indicate the square is no longer associated with a ship.
                grid[x, y] = 0;
            }
            else
            {
                //If it does not contain a ship - notify the user.
                Console.WriteLine("\nYour shot was off the mark, no ship was harmed!\n");

                //Also update the user's shot counter.
                this.shotsMissed = this.shotsMissed + 1;
            }

        }
       public bool playerHasWonCheck(int ship1Squares, int ship2Squares, int ship3Squares)
        {
            //A boolean used to indicate completion of the game.
            bool playerWon;
            //Sum the total values of the squares.
            if(ship1Squares+ship2Squares+ship3Squares==0)
            {
                //If the total value is zero, this shows no ships are remaining. Update the boolean value to reflect this information.
                playerWon = true;
            }
            else
            {
                //Ship(s) may be remaining on the grid.
                playerWon = false;
            }
            //Return the resultant boolean to the main function.
            return playerWon;
        }
      
     
    }
}
