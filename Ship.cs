﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships
{
    class Ship
    {
        //Attributes
        public int shipSize;
        //Constructor
        public Ship(char shipType)
        {
            //Battleship
            if(shipType == 'b')
            {
                shipSize = 5;

            }
            //Destroyers
            else
            {
                shipSize = 4;
            }

        }
        public void placeShip(Random rnd, int[,] grid, int shipNumber)
        {
            //Random a starting X/Y position for the ship.
            int shipX = rnd.Next(0, 9);
            int shipY = rnd.Next(0, 9);
            
            //Perform a test on the current grid to avoid ships overlapping.
            bool test = testShipPlacement(grid, shipX, shipY);

            //If there is a conflict of paths, re-random the ship location, and try the test again, until a valid track becomes available. 
            while(test == false)
            {
                shipX = rnd.Next(0, 9);
                shipY = rnd.Next(0, 9);
                test = testShipPlacement(grid, shipX, shipY);
            }
            // If the starting position is in the upper left corner...
            if(shipX <= 4 && shipY <= 4)
            {
                //Place the ships squares on the grid dependent upon its size dictated by its type.
                for(int i = 0; i < shipSize; i++)
                {
                    //Place the respective ship number assigned to the object in the grid.
                    grid[shipX, shipY] = shipNumber;

                    //Increment the ships Y co-ordinate for the next loop.
                    shipY = shipY + 1;
                }
            }
            // If the starting position is in the lower left corner...
            else if(shipX <= 4 && shipY >= 4)
            {
                //Place the ships squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //Place the respective ship number assigned to the object in the grid.
                    grid[shipX, shipY] = shipNumber;

                    //Increment the ships X co-ordinate for the next loop.
                    shipX = shipX + 1;
                }
            }
            // If the starting position is in the upper right corner...
            else if(shipX >= 4 && shipY <=4)
            {
                //Place the ships squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //Place the respective ship number assigned to the object in the grid.
                    grid[shipX, shipY] = shipNumber;

                    //Decrement the ships X co-ordinate for the next loop.
                    shipX = shipX - 1;
                }
            }
            // If the starting position is in the lower right corner...
            else if(shipX >= 4 && shipY >= 4)
            {
                //Place the ships squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //Place the respective ship number assigned to the object in the grid.
                    grid[shipX, shipY] = shipNumber;

                    //Decrement the ships Y co-ordinate for the next loop.
                    shipY = shipY - 1;
                }
            }     

        }
        public bool testShipPlacement(int[,] gridCurrent, int x, int y)
        {
            //Boolean used to report if the potential route is okay i.e. not conflicting with another ship.
            bool shipOkay = true;

            // If the starting position is in the upper left corner...
            if(x <= 4 && y <= 4)
            {
                    //Place the test squares on the grid dependent upon its size dictated by its type.
                    for(int i = 0; i < shipSize; i++)
                    {
                        //If there is already a ship plotted on this square.
                        if(gridCurrent[x,y]!= 0)
                        {
                            //Set the boolean value to false, and break the loop.
                            shipOkay = false;
                            break;
                        }
                        //Increment the test positions Y co-ordinate to mimic ship placement
                        y = y + 1;
                    }
                
            }
            else if(x <= 4 && y >= 4)
            {
                //Place the test squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //If there is already a ship plotted on this square.
                    if (gridCurrent[x, y] != 0)
                    {
                        //Set the boolean value to false, and break the loop.
                        shipOkay = false;
                        break;
                    }
                    //Increment the test positions X co-ordinate to mimic ship placement
                    x = x + 1;
                }
            }
            else if(x >= 4 && y <= 4)
            {
                //Place the test squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //If there is already a ship plotted on this square.
                    if (gridCurrent[x, y] != 0)
                    {
                        //Set the boolean value to false, and break the loop.
                        shipOkay = false;
                        break;
                    }
                    //Decrement the test positions X co-ordinate to mimic ship placement
                    x = x - 1;
                }
            }
            else if(x >= 4 && y >= 4)
            {
                //Place the test squares on the grid dependent upon its size dictated by its type.
                for (int i = 0; i < shipSize; i++)
                {
                    //If there is already a ship plotted on this square.
                    if (gridCurrent[x, y] != 0)
                    {
                        //Set the boolean value to false, and break the loop.
                        shipOkay = false;
                        break;
                    }
                    //Decrement the test positions Y co-ordinate to mimic ship placement
                    y = y - 1;
                }
            }
            //Return a boolean value to the ship placement method, in order to detemine whether a reroll is required on the starting position.
            return shipOkay;
        }
        public int checkIfShipDestroyed(int[,] grid,int shipNumber)
        {
            //A variable to temporarily hold the ships remaining occupied squares.
            int shipSquaresLeft = 0;
            //Check if the ship has any squares left occupied.
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    //If the number matches on the grid...
                    if(grid[i,j] == shipNumber)
                    {
                        //Increment the square counter.
                        shipSquaresLeft = shipSquaresLeft + 1;
                    }
                } 
            }
            //Perform a check to see if the ship is fully destroyed.
            if (shipSquaresLeft == 0)
            {
                //Depending on the ship number, a different message is outputted to the user.
                switch(shipNumber)
                {
                    case 1:
                        Console.WriteLine("A battleship has been destroyed");
                        break;

                    default:
                        Console.WriteLine("A destroyer has been sunk");
                        break;

                }

            }
            //Return the current number of remaining squares left for a certain ship.
            return shipSquaresLeft;
        }
    }
}
