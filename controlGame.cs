﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Battleships
{
    /*
        You should create a simple console application to allow a single human player to play a one-sided
        game of battleships against the computer. The program should create a 10x10 grid, and place a
        number of ships on the grid at random with the following sizes:
         1x Battleship (5 squares)
         2x Destroyers (4 squares)
        The console application should accept input from the user in the format “A5” to signify a square to
        target, and feedback to the user whether the shot was a success, and additionally report on the
        sinking of any vessels. Do not spend any time formatting output in the console window (displaying a
        grid etc) - focus on the domain rather than the presentation
     */

    class controlGame
    {
        
        static void Main(string[] args)
        {
            try
            {
                //Instatiate Grid
                Grid newGrid = new Grid();

                //Create a new player object  
                Player playerCurrent = new Player();

                //Set the current  to !over
                Boolean gameOver = false;

                //Create a random object, which is passed each time when placing a ship to avoid issues with seeding.
                Random rnd = new Random();

                //Populate Grid with Ships  
                Ship ship1 = new Ship('b');
                ship1.placeShip(rnd, newGrid.currentGrid, 1);

                Ship ship2 = new Ship('d');
                ship2.placeShip(rnd, newGrid.currentGrid, 2);

                Ship ship3 = new Ship('d');
                ship3.placeShip(rnd, newGrid.currentGrid, 3);

                //TESTING GRID CONTENT - COMMENT OUT IF YOU DON'T WISH TO SEE THIS BUT IT USEFUL FOR TESTING

                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        Console.Write(newGrid.currentGrid[i, j]);
                    }
                    Console.Write(Environment.NewLine);
                }

                //TESTING GRID CONTENT ENDS HERE


                while (gameOver == false)
                {
                    //Notify the user on the number of current shots taken.
                    Console.WriteLine("Shots Missed: {0}\nShots Hit: {1}", playerCurrent.getShotsMissed(), playerCurrent.getShotsHit());

                    //Ask the user for his firing spot to hit a ship.
                    Console.Write(Environment.NewLine);
                    Console.WriteLine("Please enter a co-ordinate to fire upon between A(0) and J(9) for X, and 0-9 for Y. i.e. A1");

                    //Read in the input from the user converting to lowercase.
                    string fireSpot = Console.ReadLine().ToLower();

                    //Convert the string input into a separate char array.
                    char[] convertedShot = playerCurrent.convertShotPlacementToChar(fireSpot);

                    //Convert back the character array to an integer array to be used for fire digits. 
                    int[] toIntArray = Array.ConvertAll(convertedShot, c => (int)Char.GetNumericValue(c));

                    //Fire the shot at the location specified.
                    playerCurrent.fireShot(toIntArray[1], toIntArray[0], newGrid.currentGrid);

                    //Retrieve how many squares are remaining for each ship.
                    int ship1RemainingSquares = ship1.checkIfShipDestroyed(newGrid.currentGrid, 1);
                    int ship2RemainingSquares = ship2.checkIfShipDestroyed(newGrid.currentGrid, 2);
                    int ship3RemainingSquares = ship3.checkIfShipDestroyed(newGrid.currentGrid, 3);

                    //If all ships are destroyed, the game status will be set to over.
                    gameOver = playerCurrent.playerHasWonCheck(ship1RemainingSquares, ship2RemainingSquares, ship3RemainingSquares);

                    //Notify the user that the game is over.
                    if (gameOver == true)
                    {
                        Console.WriteLine("You have succesfully sunk all enemy ships! GG");
                    }
                }
            }
            catch(Exception ex)
            {
                //Output error message.
                Console.WriteLine(ex.Message);
            }
          //For debugging purposes.
          Console.ReadLine();
        }
      
            
    }
}
