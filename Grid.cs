﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships
{
    class Grid
    {
        public int[,] currentGrid;
        //Constructor.
        public Grid()
        {
            //Instatiate a new multidimensional array.
            currentGrid = new int[10,10];
            //Populate the grid with zeroes which are used for checking purposes. 
            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j <10; j++)
                {
                    currentGrid[i, j] = 0;
                }
            }
        }

    }
}
